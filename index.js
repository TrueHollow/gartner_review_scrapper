const logger = require('./logger')('index.js');
const IO = require('./service/IO');
const ParserMarkets = require('./service/ParserMarkets');
const ParserVendors = require('./service/ParserVendors');
const ParserReviews = require('./service/ParserReviews');

logger.info('Script is started');

const main = async () => {
  await IO.Init();
  const allMarkets = await ParserMarkets.GetAllMarkets();
  await IO.SaveMarkets(allMarkets);
  const customMarkets = [
    {
      seoName: 'analytics-business-intelligence-platforms',
    },
  ];

  // eslint-disable-next-line no-restricted-syntax
  for (const market of customMarkets) {
    // for (const market of allMarkets) {
    // eslint-disable-next-line no-await-in-loop
    const allVendors = await ParserVendors.GetAllVendors(market);
    // eslint-disable-next-line no-restricted-syntax
    for (const vendor of allVendors) {
      // eslint-disable-next-line no-await-in-loop
      const reviews = await ParserReviews.GetAllReviews(market, vendor);
      IO.SaveResult(reviews);
    }
  }
};

main().then(() => {
  logger.info('Script is finished.');
  return IO.EndResult();
});
