const logger = require('../logger')('service/ParserVendors.js');
const Net = require('./Net');

class ParserVendors {
  static async GetAllVendors(market) {
    const { seoName } = market;
    const apiEndpoint = `https://www.gartner.com/reviews/api-proxy/reviews/markets/${seoName}/vendors`;
    logger.debug('Get all vendors for (%s)', seoName);
    const json = await Net.GetJson(apiEndpoint);
    if (json.status !== 'success') {
      throw new Error(`Wrong api status (${JSON.stringify(json)})`);
    }
    const { vendor } = json;
    logger.info('Found (%d) vendors for (%s).', vendor.length, seoName);
    return vendor;
  }
}

module.exports = ParserVendors;
