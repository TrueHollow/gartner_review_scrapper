const logger = require('../logger')('service/ParserReviews.js');
const Net = require('./Net');
const config = require('../config');

const API_ENDPOINT =
  'https://www.gartner.com/reviews/api-proxy/reviews/market/vendor/filter';

const TEMPLATE = {
  vendorName: '',
  marketName: '',
  startIndex: 1,
  endIndex: 10,
  filter: {
    months: [],
    products: [],
    reviewRating: [],
    industry: [],
    jobRole: [],
    companySize: [],
    deploymentRegion: [],
  },
  sort: '-helpfulness',
  // productName: '3d-systems-on-demand-manufacturing',
};

const postProcess = d => {
  const data = d;
  if (Array.isArray(data)) {
    // eslint-disable-next-line no-restricted-syntax
    for (const item of data) {
      postProcess(item);
    }
  } else if (typeof data === 'object' && data !== null) {
    const entries = Object.entries(data);
    // eslint-disable-next-line no-restricted-syntax
    for (const [key, value] of entries) {
      if (key === 'questions') {
        delete data[key];
      } else if (
        Array.isArray(value) ||
        (typeof value === 'object' && value !== null)
      ) {
        postProcess(value);
      } else if (typeof value !== 'string' || value.length === 1) {
        delete data[key];
      }
    }
  }

  return data;
};

const filtering = jsonObject => {
  const { reviewPresentation } = jsonObject;
  const { userProfile } = reviewPresentation;
  const productNames = reviewPresentation.products.map(product => product.name);
  const sectionsWithoutQuestions = reviewPresentation.survey.sections.map(s => {
    const section = s;
    delete section.questions;
    return section;
  });
  const sections = postProcess(sectionsWithoutQuestions);
  // const review = postProcess(reviewPresentation.review);
  const { review } = reviewPresentation;
  const presentation = postProcess(reviewPresentation.presentation);
  const survey = {
    name: reviewPresentation.survey.name,
    sections,
  };
  return {
    userProfile,
    productNames,
    // vendorList: reviewPresentation.vendorList,
    review,
    survey,
    presentation,
  };
};

class ParserReviews {
  static async GetFullReviews(idArray) {
    const { RequestsLimit } = config.Parser;
    const copy = idArray.slice();
    let result = [];
    do {
      const iteration = copy.splice(0, RequestsLimit);
      const promises = iteration.map(async id => {
        const url = `https://www.gartner.com/reviews/api-proxy/reviews/review/presentationview/${id}`;
        const originaJson = await Net.GetJson(url);
        return filtering(originaJson);
      });
      // eslint-disable-next-line no-await-in-loop
      const iterationResult = await Promise.all(promises);
      result = result.concat(iterationResult);
    } while (copy.length);
    return result;
  }

  static async GetAllReviews(market, vendor) {
    const marketName = market.seoName;
    const vendorName = vendor.seoName;
    logger.debug('Get all reviews for (%s - %s).', marketName, vendorName);
    let notFinished = true;
    let count = 0;
    let step = 0;
    let reviewsIds = [];
    let userReviewsSimple = [];
    do {
      count += TEMPLATE.endIndex;
      const copy = { ...TEMPLATE };
      copy.vendorName = vendorName;
      copy.marketName = marketName;
      copy.startIndex += step;
      copy.endIndex += step;
      // eslint-disable-next-line no-await-in-loop
      const json = await Net.PostJsonAsForm(API_ENDPOINT, copy);
      if (json.totalCount <= count) {
        notFinished = false;
      }
      const iterationIds = json.userReviews.map(review => review.reviewId);
      userReviewsSimple = userReviewsSimple.concat(json.userReviews);
      reviewsIds = reviewsIds.concat(iterationIds);
      step += 10;
    } while (notFinished);

    const userReviewsFull = (await ParserReviews.GetFullReviews(
      reviewsIds
    )).map(r => {
      const review = r;
      review.marketName = marketName;
      review.vendorName = vendorName;
      return review;
    });
    logger.debug(
      '(%s - %s): %d count',
      marketName,
      vendorName,
      userReviewsFull.length
    );

    // return { userReviewsSimple, userReviewsFull };
    return userReviewsFull;
  }
}

module.exports = ParserReviews;
