const logger = require('../logger')('service/ParserMarkets.js');
const Net = require('./Net');

const API_ENDPOINT =
  'https://www.gartner.com/reviews/api-proxy/reviews/api/markets';

class ParserMarkets {
  static async GetAllMarkets() {
    logger.debug('Get all markets');
    const json = await Net.GetJson(API_ENDPOINT);
    if (json.status !== 'success') {
      throw new Error(`Wrong api status (${JSON.stringify(json)})`);
    }
    const { marketsList } = json;
    logger.info('Found (%d) markets.', marketsList.length);
    return marketsList;
  }
}

module.exports = ParserMarkets;
